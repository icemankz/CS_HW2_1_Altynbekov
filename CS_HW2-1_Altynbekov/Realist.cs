﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_1_Altynbekov
{
    class Realist : Person
    {
        public Realist()
        {

        }

        public override void Info()
        {
            base.Info();
            Console.WriteLine("я реалист(ка)");
        }

        public override void Hello(Person p)
        {
            int _age = p.Age + 5;
            if (p.Age <=this.Age || this.Age < _age)
            {
                Console.WriteLine($"{this.Name} : Привет, { p.Name}!");
            }
            else
            {
                Console.WriteLine($"{this.Name} : Здравствуй, { p.Name}.");
            }
            
        }
    }
}
