﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CS_HW2_1_Altynbekov
{
    public abstract class Person
    {
        public string Name;
        public int Age;      
        Random randAge = new Random((int)DateTime.Now.Millisecond);

        public Person()
        {
            Name = NamesList.names[new Random((int)DateTime.Now.Millisecond).Next(NamesList.names.Count)];
            Thread.Sleep(20);
            Age = randAge.Next(20, 41);
            Thread.Sleep(20);
        }

        public virtual void Info()
        {
            if (this.Age >= 22 && this.Age <= 24 || this.Age >= 32 && this.Age <= 34)
                
            {
                Console.WriteLine("Меня зовут " + Name + ", мой возраст: - " + Age + " года");
            }
            else if (this.Age == 21 || this.Age == 31)
            {
                Console.WriteLine("Меня зовут " + Name + ", мой возраст: - " + Age + " год");
            }
            else
            {
                Console.WriteLine("Меня зовут " + Name + ", мой возраст: - " + Age + " лет");
            }
        }

        public virtual void Hello(Person p)
        {
       
        }

    }
}
