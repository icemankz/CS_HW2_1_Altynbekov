﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CS_HW2_1_Altynbekov
{
    class Formalist : Person
    {       
        public Formalist()
        {
            
        }

        public override void Info()
        {
            base.Info();
            Console.WriteLine("я формалист(ка)");
        }

        public override void Hello(Person p)
        {
            Console.WriteLine($"{this.Name} : Здравствуй, { p.Name}.");
        }
    }
}
