﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_1_Altynbekov
{
    class Program
    {
        static void Main(string[] args)
        {
            
            List<Person> persons = new List<Person>()
            {
                new Formalist(),
                new Informal(),
                new Realist(),
                new Informal(),
                new Formalist(),
            };
            
            foreach (var person in persons)
            {
                person.Info();                
            }

            Console.WriteLine();
            for (int i = 0; i < persons.Count; i++)
            {
                Console.WriteLine();
                for (int j = 0; j < persons.Count; j++)
                {
                    if (persons.ElementAt(i) != persons.ElementAt(j))
                    {
                        persons.ElementAt(i).Hello(persons.ElementAt(j));
                        persons.ElementAt(j).Hello(persons.ElementAt(i));
                    }
                }
            }         
        }

    }
}
