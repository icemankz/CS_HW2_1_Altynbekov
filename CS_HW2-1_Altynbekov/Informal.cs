﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_1_Altynbekov
{
    class Informal : Person
    {
        public Informal()
        {

        }

        public override void Info()
        {
            base.Info();
            Console.WriteLine("я неформал(ка)");
        }

        public override void Hello(Person p)
        {

            Console.WriteLine($"{this.Name} : Привет, { p.Name}!");
        }
    }
}
